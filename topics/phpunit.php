<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 18-Jan-18
 * Time: 12:13 PM
 */

require_once __DIR__ . '/../lib/Core.php';
require_once __DIR__ . '/../lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('PHPUnit');
echo Standard::navbar('PHPUnit');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <h1 class="display-4">PHPUnit</h1>
                <p class="lead">Good Testers are very Assertive</p>
                <hr class="my-4">
                <p>
                    PHPUnit is a framework for white-box testing your PHP code. PHPUnit is much like JUnit in that
                    we write test functions with asserts. The most basic tests we can write at tests that ensure
                    data layer classes are behaving correctly. For the moment, we'll just want to write test cases for
                    our
                    models. In order to organize our test cases, we'll want to follow a few rules.
                </p>
                <ul>
                    <li>Every PHPUnit test class should be inside of a <code>tests</code> directory.</li>
                    <li>Every PHPUnit test class should be named <code>ClassTest</code> (e.g. CoffeeTest)</li>
                    <li>Every PHPUnit test class file name should match the class name</li>
                    <li>The tests directory should mirror the structure of the root directory</li>
                </ul>
                <p>
                    With these rules in mind, we have a <code>~/tests</code> directory that contains <code>CoffeeTest.php</code>.
                </p>
                <pre>
                <code class="border border-white rounded">
<?php echo htmlentities(file_get_contents(__DIR__ . '/../tests/CoffeeTest.php')); ?>
                </code>
                </pre>
                <p>
                    Since models don't have any logic associated with them, their tests are relatively straightforward.
                    In general, there should be one test function for every function in the class under test.
                </p>
            </div>
        </div>
    </div>
</div>
<script src="https://delorean.challstrom.com/scripts/hljsLoader.js"
        integrity="sha384-L02yBxfn8RHfHRN5QAniy4Rm/V0Jml0UagqMZjy00OcWwO2Yg8EhWbH+iOF+zkm0" crossorigin="anonymous"
        defer></script>
<?php echo Standard::footer() ?></body>
</html>

