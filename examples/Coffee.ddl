CREATE TABLE Coffee
(
  coffee_id   INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  species     VARCHAR(14),
  caffeine    FLOAT                        DEFAULT '0',
  isRoasted   TINYINT(1)                   DEFAULT '0',
  isSpecialty TINYINT(1)                   DEFAULT '0'
);