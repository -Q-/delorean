<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 22-Jan-18
 * Time: 03:57 PM
 */
require_once __DIR__ . '/Drinkable.php';

/**
 * Class Coffee
 */
class Coffee extends Drinkable
{
    /**
     * @var int
     */
    public $coffee_id;
    /**
     * @var string
     */
    public $species;
    /**
     * @var float
     */
    public $caffeine;
    /**
     * @var bool
     */
    public $isSpecialty;

    /**
     * @var bool
     */
    public $isRoasted;

    /**
     * Coffee constructor.
     * @param int $coffee_id
     * @param string $species
     * @param float $caffeine
     * @param bool $isSpecialty
     * @param bool $isRoasted
     */
    public function __construct(int $coffee_id, string $species, float $caffeine, bool $isSpecialty, bool $isRoasted)
    {
        $this->coffee_id = $coffee_id;
        $this->species = $species;
        $this->caffeine = $caffeine;
        $this->isSpecialty = $isSpecialty;
        $this->isRoasted = $isRoasted;
    }

    /**
     * @return int
     */
    public function getCoffeeId(): int
    {
        return $this->coffee_id;
    }

    /**
     * @return string
     */
    public function getSpecies(): string
    {
        return $this->species;
    }

    /**
     * @return float
     */
    public function getCaffeine(): float
    {
        return $this->caffeine;
    }

    /**
     * @return bool
     */
    public function isSpecialty(): bool
    {
        return $this->isSpecialty;
    }

    /**
     * @return bool
     */
    public function isRoasted(): bool
    {
        return $this->isRoasted;
    }

    /**
     *
     */
    public function roast()
    {
        $this->isRoasted = true;
        $this->temperature = 85.1;
    }

}