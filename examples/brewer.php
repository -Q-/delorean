<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 31-Jan-18
 * Time: 11:53 PM
 */
require_once __DIR__ . '/Drinkable.php';
require_once __DIR__ . '/Coffee.php';
require_once __DIR__ . '/Cup.php';
require_once __DIR__ . '/Roaster.php';

$coffee = new Coffee(0, "Arabica", 0.012, true, false);
Roaster::roastCoffee($coffee);
$cup = new Cup();
$cup->addContents($coffee);
echo json_encode($cup);