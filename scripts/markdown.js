/*
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */
$.get('https://delorean.challstrom.com/examples/api/documentation.md', '',
    function (data) {
        showdown.setFlavor('github');
        const converter = new showdown.Converter({'tables': true, 'simplifiedAutoLink': true}),
            text = data,
            html = converter.makeHtml(text);
        $('#renderedMarkdown').html(html);
    });
