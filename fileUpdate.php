<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 27-Feb-18
 * Time: 03:10 PM
 */

$data = json_encode(dirToHashArray('.'));
file_put_contents('fileStatus.json', $data);
echo $data;

function dirToHashArray($dir)
{

    $result = array();

    $cdir = scandir($dir);
    foreach ($cdir as $key => $value) {
        if (!in_array($value, array(".", ".."))) {
            $fullName = $dir . DIRECTORY_SEPARATOR . $value;
            if (is_dir($fullName)) {
                $result[$value] = dirToHashArray($fullName);
            } else {
                $result[$value] = md5_file($fullName);
            }
        }
    }

    return $result;
}