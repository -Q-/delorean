<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 27-Feb-18
 * Time: 03:38 PM
 */
require_once __DIR__ . '/protected/SessionManagement.php';
$statusString = file_get_contents(__DIR__ . '/fileStatus.json');
FileStatus::$currentStatus = json_decode($statusString, true);
if (SessionManagement::getSessionValue('fileStatus'))
    FileStatus::$givenStatus = json_decode(SessionManagement::getSessionValue('fileStatus'), true);
else
    FileStatus::$givenStatus = FileStatus::$currentStatus;
SessionManagement::updateSession('fileStatus', $statusString);

class FileStatus
{
    public static $currentStatus = [];
    public static $givenStatus = [];

    /**
     * @param $dir
     * @return string
     * @deprecated Use getAnchor()
     */
    public static function getClass($dir): string
    {
        if (self::isFileChanged($dir)) {
            return 'changed';
        }
        return '';
    }

    public static function isFileChanged($dir): bool
    {
        $self = __DIR__ . '/';
        $dir = realpath($dir);
        if (FileStatus::$givenStatus == []) {
            return true;
        }
        if (!file_exists($dir)) {
            error_log("FILE $dir NOT FOUND");
            return false;
        }
        $dir = str_replace($self, '', $dir);
        $steps = explode(DIRECTORY_SEPARATOR, $dir);
        $currentHash = '';
        $givenHash = '';
        if (in_array($dir, array_keys(self::$currentStatus))) {
            $currentHash = self::$currentStatus[$dir];
        } else {
            $next = self::$currentStatus;
            foreach ($steps as $step) {
                if (is_array($next[$step])) {
                    $next = $next[$step];
                } else {
                    $currentHash = $next[$step];
                    break;
                }
            }
        }
        if (in_array($dir, array_keys(self::$givenStatus))) {
            $givenHash = self::$givenStatus[$dir];
        } else {
            $next = self::$givenStatus;
            foreach ($steps as $step) {
                if (is_array($next[$step])) {
                    $next = $next[$step];
                } else {
                    $givenHash = $next[$step];
                    break;
                }
            }
        }
        //echo $dir . '->' . $currentHash . '=?' . $givenHash . PHP_EOL;
        return $currentHash != $givenHash;
    }


    /**
     * @param $dir
     * @return string
     * @deprecated Use getAnchor()
     */
    public static function getSpan($dir): string
    {
        if (self::isFileChanged($dir)) {
            return '<span class="label label-success">New</span>';
        }
        return '';
    }

    public static function getAnchor($dir, $innerText, $classes, $target, $optionalAttributes = ''): string
    {
        $self = 'https://delorean.challstrom.com/';
        $dir = str_replace($self, '', $dir);
        $id = $innerText . 'Anchor';
        if (!file_exists(__DIR__ . DIRECTORY_SEPARATOR . $dir)) {
//            echo "FILE $dir NOT FOUND<br>";
            require_once __DIR__ . '/protected/Log.php';
            $dir = __DIR__ . DIRECTORY_SEPARATOR . $dir;
            Log::error("FILE $dir NOT FOUND", __LINE__);
            return "<a href='$self$dir' id='$id' class='$classes disabled' target='$target' $optionalAttributes>$innerText</a>";
        }
        if (self::isFileChanged(__DIR__ . DIRECTORY_SEPARATOR . $dir)) {
            return "<a href='$self$dir' id='$id' class='$classes changed' target='$target' $optionalAttributes>$innerText&nbsp<span class='badge'>New</span></a>";
        } else {
            return "<a href='$self$dir' id='$id' class='$classes' target='$target' $optionalAttributes>$innerText</a>";
        }
    }
}