<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 18-Jan-18
 * Time: 12:13 PM
 */

require_once __DIR__ . '/../lib/Core.php';
require_once __DIR__ . '/../lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('Javascript');
echo Standard::navbar('Javascript');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <h1 class="display-4">Javascript</h1>
                <p class="lead">It may not be the language we wanted, but it's the language we deserve.</p>
                <hr class="my-4">
                <p>
                    JavaScript is a programming language that we'll be using to add client-side functionality.
                    JavaScript
                    supports Object Oriented, Imperative, and Functional programming styles. The type of client-side
                    functionality will differ from solution to solution, but we'll probably be using it to modify
                    the DOM (a fancy CS word for a web page) and to make requests to our servers (Think AJAX).
                    <br>
                    Because JavaScript is <em>an entire language</em> we won't be covering all its ins, outs, and
                    syntax.
                    If you wish to take a deep-dive into JavaScript W3 Schools has a fantastic series of tutorials
                    and documentations <a href="https://www.w3schools.com/js/" target="_blank" rel="nofollow noopener">here.</a>
                    What we will be going over is different scripts that we can modify and use to interact with our
                    websites.
                </p>
                <ul>
                    <li><a href="#dom" rel="tag">Modifying the DOM</a></li>
                    <li><a href="#ajax" rel="tag">Making AJAX Requests</a></li>
                    <li><a href="#formData" rel="tag">Serializing Form Data</a></li>
                </ul>
                <h4 class="">Modifying the DOM</h4>
                <a name="dom"></a>
                <p class="lead"></p>
                <hr class="my-4">
                <p>
                    Whenever we're using client-side JavaScript, we'll almost always be modifying the DOM. This
                    modification
                    can be anything from changing the color of a button to changing the style of an entire web page.
                    We primarily modify the DOM by selecting a single element with an identifier, or <code>id</code>,
                    and
                    changing its attributes.
                    The <code>id</code> of a given element on a page should be <strong>unique</strong>.
                    So if we have a button with <code>id='coffeeButton1'</code> then we cannot have another
                    button on the same page with the same <code>id</code>. As such, if we have a second button, we ought
                    to have an
                    <code>id</code> like <code>id='coffeeButton2'</code>. So the <em>HTML</em> of our web page would
                    look like the following:
                </p>
                <pre>
                <code class="border border-white rounded">
&lt;button id='coffeeButton1'&gt;Pour Coffee 1&lt;/button&gt;
&lt;button id='coffeeButton2'&gt;Pour Coffee 2&lt;/button&gt;
                </code>
                </pre>
                <form class='form-inline border border-white rounded' style="padding: 2rem; margin-bottom: 5rem;">
                    <button class="btn btn-primary" id="coffeeButton1" type="button" style="width: 50%;">Pour Coffee 1
                    </button>
                    <button class="btn btn-success" id="coffeeButton2" type="button" style="width: 50%;">Pour Coffee 2
                    </button>
                </form>
                <p>
                    If we have unique identifiers for every element on the page we're ready to start modifying
                    individual elements on the DOM. Let's look at a script that will change the color and <code>innerHTML</code>
                    of a button when it's pressed.
                </p>
                <pre>
                <code class="border border-white rounded">
&lt;button type='button' style='background-color: limegreen;' id='happySadButton' onclick='toggleButton()'&gt;Happy!&lt;/button&gt;
&lt;script type="application/javascript"&gt;
    function toggleButton() {
        const happySadButton = document.getElementById('happySadButton');
        if (happySadButton.innerHTML === 'Happy!') {
            happySadButton.innerHTML = 'Sad.';
            happySadButton.style.backgroundColor = 'lightskyblue';
        }
        else {
            happySadButton.innerHTML = 'Happy!';
            happySadButton.style.backgroundColor = 'limegreen';
        }
    }
&lt;/script&gt;
                </code>
                </pre>
                <form class='form-inline border border-white rounded' style="padding: 2rem; margin-bottom: 5rem;">
                    <button type='button' class="btn btn-default" style='background-color: limegreen; width: 100%;'
                            id='happySadButton' onclick='toggleButton()'>Happy!
                    </button>
                </form>
                <script type="application/javascript">
                    function toggleButton() {
                        const happySadButton = document.getElementById('happySadButton');
                        if (happySadButton.innerHTML === 'Happy!') {
                            happySadButton.innerHTML = 'Sad.';
                            happySadButton.style.backgroundColor = 'lightskyblue';
                        }
                        else {
                            happySadButton.innerHTML = 'Happy!';
                            happySadButton.style.backgroundColor = 'limegreen';
                        }
                    }
                </script>
                <p>
                    We can modify any element on the page in almost any way in response to events.
                    Events are any type of change that has happened on the page, such as a button being pressed,
                    scrolling the page, or a timer or interval triggering.
                </p>
                <h4 class="">Making AJAX Requests</h4>
                <a name="ajax"></a>
                <p class="lead"></p>
                <hr class="my-4">
                <p>
                    AJAX request are Asynchronous JavaScript And XML requests. They are a way to submit and receive
                    data from our web server <em>after the page has loaded and without reloading the page</em>.
                    Vanilla JavaScript AJAX requests are rather cumbersome to use, especially for beginners, so we're
                    going to use <a href="https://jquery.com/" target="_blank" rel="nofollow noopener">jQuery</a>.
                    jQuery will handle all of
                    the messiness for us, so we can get right to functionality.
                    <br>
                    Here is an example of an ajax request that loads information via an HTTPS GET request from
                    our <u>Coffee API</u>.
                </p>
                <button class="btn btn-info" onclick="getCoffeeInformation()">Load Coffee Info</button>
                <pre>
                <code id="coffeeGetArea" class="border border-white rounded json">

                </code>
                </pre>
                <script type="application/javascript">
                    function getCoffeeInformation() {
                        const coffeeGetArea = $('#coffeeGetArea');
                        $.ajax({
                            url: "https://delorean.challstrom.com/examples/api/coffee.php",
                            type: "GET",
                            data: "",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                /** @namespace response.data */
                                    //N.B. JavaScript will **automatically** parse JSON strings into Objects!
                                let jsonString = JSON.stringify(response.data, null, 2);
                                coffeeGetArea.html(jsonString);
                                coffeeGetArea.height(jsonString.length / (coffeeGetArea.width() / 16) + "rem");
                                hljs.highlightBlock(coffeeGetArea[0]);
                            },
                            error: function (data, status, error) {
                                console.log(data, status, error);
                                coffeeGetArea.html("Whoops! We've encountered an error!");
                            }
                        });
                    }
                </script>
                <p>AJAX HTML</p>
                <pre>
                <code class="border border-white rounded">
&lt;button onclick="getCoffeeInformation()"&gt;Load Coffee Info&lt;/button&gt;
&lt;label for="coffeeGetArea"&gt;Coffee Info&lt;/label&gt;
&lt;textarea id="coffeeGetArea"&gt;Coffee Goes Here!&lt;/textarea&gt;
                </code>
                </pre>
                <p>AJAX JavaScript (jQuery)</p>
                <pre>
                <code class="border border-white rounded">
//This function will modify a textarea's value attribute and fill it with coffee stuff
function getCoffeeInformation() {
const coffeeGetArea = $('#coffeeGetArea');
//Let's do a AJAX GET request to the coffee api, we want all of the coffees so we won't include any
//request information
//since we're using JSON, we need to make sure to set the contentType to application/json and
//our dataType to json
$.ajax({
    url: "https://delorean.challstrom.com/examples/api/coffee.php",
    type: "GET",
    data: "",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (response) {
        /** @namespace response.data */
        //N.B. JavaScript will **automatically** parse JSON strings into Objects!
        let jsonString = JSON.stringify(response.data, null, 2);
        coffeeGetArea.val(jsonString);
        coffeeGetArea.height(jsonString.length / (coffeeGetArea.width()/20) + "rem");
    },
    error: function (data, status, error) {
        console.log(data, status, error);
        coffeeGetArea.val("Whoops! We've encountered an error!");
    }
});
}
                </code>
                </pre>
                <h4 class="">Serializing Form Data</h4>
                <a name="formData"></a>
                <p class="lead"></p>
                <hr class="my-4">
                <p>
                    Serializing form data into JSON is easy with jQuery. The only information jQuery needs to serialize
                    our form into a JSON string is a unique identifier on every form element that has data.
                </p>
                <form id="coffeeForm" onsubmit="putCoffee(this)" autocomplete="off">
                    <div class="form-group">
                        <label for="coffee_id">coffee_id</label>
                        <input type="number" class="form-control" min="0" step="1" id="coffee_id" name="coffee_id"
                               value="2">
                    </div>
                    <div class="form-group">
                        <label for="species">species</label>
                        <input type="text" class="form-control" id="species" placeholder="Liberica" required>
                    </div>
                    <div class="form-group">
                        <label for="caffeine">caffeine</label>
                        <input type="number" class="form-control" min="0" step="0.01" id="caffeine" placeholder="0.05"
                               required>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="isRoasted">
                        <label class="form-check-label" for="isRoasted">isRoasted</label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="isSpecialty">
                        <label class="form-check-label" for="isSpecialty">isSpecialty</label>
                    </div>
                    <br>
                    <button class="btn btn-primary" style="width: 100%;" id="putCoffeeButton">PUT Coffee</button>
                </form>
                <script type="application/javascript">
                    document.addEventListener("DOMContentLoaded", function (event) {
                        $("form").submit(function (e) {
                            e.preventDefault(e);
                        });
                    });

                    function putCoffee(form) {
                        let keyValueArray = {};
                        keyValueArray['coffee_id'] = $('#coffee_id').val();
                        keyValueArray['species'] = $('#species').val();
                        keyValueArray['caffeine'] = $('#caffeine').val();
                        keyValueArray['isRoasted'] = $('#isRoasted').is(":checked");
                        keyValueArray['isSpecialty'] = $('#isSpecialty').is(":checked");
                        let coffeeJSONString = JSON.stringify(keyValueArray);
                        const putCoffeeButton = $('#putCoffeeButton');
                        $.ajax({
                            url: "https://delorean.challstrom.com/examples/api/coffee.php",
                            type: "PUT",
                            data: coffeeJSONString,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                /** @namespace response.data
                                 *  @namespace response.status
                                 */
                                //N.B. JavaScript will **automatically** parse JSON strings into Objects!
                                if (response.status === "SUCCESS") {
                                    putCoffeeButton.removeClass("btn-primary").addClass("btn-success");
                                    putCoffeeButton.html("Put Successful!");
                                    setTimeout(function () {
                                        putCoffeeButton.removeClass("btn-success").addClass("btn-primary");
                                        putCoffeeButton.html("PUT Coffee");
                                    }, 4000);
                                }
                            },
                            error: function (data, status, error) {
                                console.log(`${data.responseJSON.messages}, ${status}, ${error}`);
                                putCoffeeButton.removeClass("btn-primary").addClass("btn-danger");
                                putCoffeeButton.html(`Oh noes! An error occurred! <small>${data.responseJSON.messages[0]}</small>`);
                                setTimeout(function () {
                                    putCoffeeButton.removeClass("btn-danger").addClass("btn-primary");
                                    putCoffeeButton.html("PUT Coffee");
                                }, 6000);
                            }
                        });
                    }
                </script>
                <br>
                <p>JavaScript for serializing and submitting form data with User feedback</p>
                <pre>
                <code class="border border-white rounded">
//We'll want to make sure any forms on the page aren't allowed to submit and reload the page
document.addEventListener("DOMContentLoaded", function (event) {
    $("form").submit(function (e) {
        e.preventDefault(e);//this will prevent forms from submitting and the page from reloading
    });
});

//this function will take in a DOMElement, hopefully of the type form
function putCoffee(form) {
    //we need to grab all of the data from our form and put it into an associative array
    let keyValueArray = {};
    keyValueArray['coffee_id'] = $('#coffee_id').val();
    keyValueArray['species'] = $('#species').val();
    keyValueArray['caffeine'] = $('#caffeine').val();
    keyValueArray['isRoasted'] = $('#isRoasted').val();
    keyValueArray['isSpecialty'] = $('#isSpecialty').val();
    //now we turn our payload into JSON
    let coffeeJSONString = JSON.stringify(keyValueArray);
    const putCoffeeButton = $('#putCoffeeButton');
    //now let's make an AJAX request to the coffee api, we want to make sure to do a PUT request
    //since we're using JSON, we need to make sure to set the contentType to application/json and
    //our dataType to json
    $.ajax({
        url: "https://delorean.challstrom.com/examples/api/coffee.php",
        type: "PUT",
        data: coffeeJSONString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            /** @namespace response.data
             *  @namespace response.status
             */
            //N.B. JavaScript will **automatically** parse JSON strings into Objects!
            //This block checks to see if the AJAX request was successful, but we still need to
            //make sure our response payload doesn't have any errors
            if (response.status === "SUCCESS") {
                putCoffeeButton.removeClass("btn-primary").addClass("btn-success");
                putCoffeeButton.html("Put Successful!");
                setTimeout(function () {
                    putCoffeeButton.removeClass("btn-success").addClass("btn-primary");
                    putCoffeeButton.html("PUT Coffee");
                }, 4000);
            }
        },
        error: function (data, status, error) {
            console.log(data, status, error);
            putCoffeeButton.removeClass("btn-primary").addClass("btn-danger");
            putCoffeeButton.html("Oh noes! An error occurred!");
            setTimeout(function () {
                putCoffeeButton.removeClass("btn-danger").addClass("btn-primary");
                putCoffeeButton.html("PUT Coffee");
            }, 6000);
        }
    });
}
                </code>
                </pre>
                <p>
                    HTML form with Bootstrap Styling
                </p>
                <pre>
                <code class="border border-white rounded">
&lt;form id="coffeeForm" onsubmit="putCoffee(this)" autocomplete="off"&gt;
&lt;div class="form-group"&gt;
    &lt;label for="coffee_id"&gt;coffee_id&lt;/label&gt;
    &lt;input type="number" class="form-control" min="0" step="1" id="coffee_id" name="coffee_id" value="2"
           disabled&gt;
&lt;/div&gt;
&lt;div class="form-group"&gt;
    &lt;label for="species"&gt;species&lt;/label&gt;
    &lt;input type="text" class="form-control" id="species" placeholder="Liberica" required&gt;
&lt;/div&gt;
&lt;div class="form-group"&gt;
    &lt;label for="caffeine"&gt;caffeine&lt;/label&gt;
    &lt;input type="number" class="form-control" min="0" step="0.01" id="caffeine" placeholder="0.05" required&gt;
&lt;/div&gt;
&lt;div class="form-check"&gt;
    &lt;input type="checkbox" class="form-check-input" id="isRoasted"&gt;
    &lt;label class="form-check-label" for="isRoasted"&gt;isRoasted&lt;/label&gt;
&lt;/div&gt;
&lt;div class="form-check"&gt;
    &lt;input type="checkbox" class="form-check-input"  id="isSpecialty"&gt;
    &lt;label class="form-check-label" for="isSpecialty"&gt;isSpecialty&lt;/label&gt;
&lt;/div&gt;
&lt;br&gt;
&lt;button class="btn btn-primary" style="width: 100%;" id="putCoffeeButton"&gt;PUT Coffee &lt;/button&gt;
&lt;/form&gt;
                </code>
                </pre>
            </div>
        </div>
    </div>
</div>
<script src="https://delorean.challstrom.com/scripts/hljsLoader.js"
        integrity="sha384-L02yBxfn8RHfHRN5QAniy4Rm/V0Jml0UagqMZjy00OcWwO2Yg8EhWbH+iOF+zkm0" crossorigin="anonymous"
        defer></script>
<?php echo Standard::footer() ?></body>
</html>
