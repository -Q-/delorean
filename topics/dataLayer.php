<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 18-Jan-18
 * Time: 12:13 PM
 */

require_once __DIR__ . '/../lib/Core.php';
require_once __DIR__ . '/../lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('The Data Layer');
echo Standard::navbar('The Data Layer');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <h1 class="display-4">The Data Layer</h1>
                <p class="lead">"I could be pursuing an untamed ornithoid without cause."</p>
                <hr class="my-4">
                <p>
                    So far we have Models and ViewControllers. We also (should) have a database. Now how do we get the
                    two to talk? We use something called the <em>data layer.</em>
                </p>
                <div class="text-center">
                    <img alt="Layered Approach" class="img-fluid" width="30%"
                         src="https://delorean.challstrom.com/images/layeredApproach1.svg">
                </div>
                <p>
                    So what does the Data layer <em>do?</em> Basically, the Data layer puts Models into the database,
                    and retrieves
                    Models from the database.
                    Let's look at an example of a data layer class that works with our Coffee model and our Coffee table
                    (shown below).
                </p>
                <pre>
                <code class="border border-white rounded">
<?php echo htmlentities(file_get_contents(__DIR__ . '/../examples/Coffee.ddl')); ?>
                </code>
                </pre>
                <pre>
                <code class="border border-white rounded">
<?php echo htmlentities(file_get_contents(__DIR__ . '/../examples/repository/CoffeeRepository.php')); ?>
                </code>
                </pre>
                <p>
                    Notice the naming pattern, (Model)Repository.php. We're using an easier version of the Repository
                    pattern, so we should
                    name it accordingly.
                    Also notice how we our class is a static class, in PHP this can be define as a class with only
                    static function
                    and is not instantiated.
                    To use our data layer class, all we would have to do is call our static functions to retrieve a
                    <code>Coffee</code> from
                    our databsae. For example, we if we wanted to find all of the Coffees of type 'Liberica' we could do
                    the following:
                    <code class="border border-white rounded">
                        $allLibericaCoffees = CoffeeRepository::getCoffeesBySpecies('Liberica');
                    </code>
                    Every database table should have a corresponding repository class. In most circumstances, every <em>one</em>
                    table will have one <em>repository</em> class.
                </p>
            </div>
        </div>
    </div>
</div>
<script src="https://delorean.challstrom.com/scripts/hljsLoader.js"
        integrity="sha384-L02yBxfn8RHfHRN5QAniy4Rm/V0Jml0UagqMZjy00OcWwO2Yg8EhWbH+iOF+zkm0" crossorigin="anonymous"
        defer></script>
<?php echo Standard::footer() ?></body>
</html>
