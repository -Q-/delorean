<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * Staff: tchallst
 * Date: 03-Jul-17
 * Time: 10:30 PM
 */
class SessionManagement
{


    /**
     * @param $key , key in _SESSION to set to $value
     * @param $value , value in _SESSION to be set at $key
     */
    public static function updateSession($key, $value)
    {
        if (session_status() == PHP_SESSION_NONE) self::startSession();
        $_SESSION[$key] = $value;
    }

    /**
     *
     */
    public static function startSession()
    {
        $lifetime = 60 * 60 * 24 * 30;
        session_set_cookie_params($lifetime);
        session_start();
    }

    /**
     * @param $key , key in _SESSION to retrieve value from
     * @return mixed, value in _SESSION retrieved at $key
     */
    public static function getSessionValue($key)
    {
        if (session_status() == PHP_SESSION_NONE) self::startSession();
        return isset($_SESSION[$key]) ? $_SESSION[$key] : false;
    }

    /**
     *
     */
    public static function destroySession()
    {
        if (session_status() == PHP_SESSION_NONE) self::startSession();
        session_destroy();
    }
}