<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 15-Jan-18
 * Time: 02:20 PM
 */

require_once __DIR__ . '/lib/Core.php';
require_once __DIR__ . '/lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('Home');
echo Standard::navbar('Home');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <p>Welcome to the 3339 / 4157 Technical Advisor Wiki! Throughout the semester you will find a number of
                    topics helpful
                    available here. The source code of this wiki is also <em>publicly available</em> on <a
                            rel="external"
                            href="https://bitbucket.org/-Q-/delorean">BitBucket</a>.
                    Although please be aware that <strong>every piece of code is copyrighted,</strong> so <em>do
                        <u>not</u> copy-paste code, <strong>especially from Bitbucket.</strong></em>
                    You may use code that is included on a page in this wiki and is marked by a white background.
                    <br>If you require additional help you can always <a rel="external"
                                                                         href="mailto:tchallst@stedwards.edu?subject=Software Engineering I" target="_blank">Email
                        your Technical Advisor</a> or
                    you can
                </p>
                <form class="" method="get" action="https://google.com/search" target="_blank">
                    <div class="form-group">
                        <div class="input-group">
                            <input title="Search Google" class="form-control" name="q"
                                   placeholder="Search Google. . . ">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">Google Search</button>
                            </div>
                        </div>
                    </div>
                </form>
                <br>
                <h4>File Status</h4>
                <hr class="my-4">
                <p>
                    This site periodically hashes all of its files and generates a <a
                            href="https://delorean.challstrom.com/fileStatus.json" target="_blank"
                            rel="help nofollow">fileStatus.json</a> file. This file
                    is copied into your session every time you load a page. If you have a session available (if you've
                    already visited the site today) then the fileStatus.json that you have in your session is
                    compared to the most up to date version of fileStatus.json on the server. For any files that
                    have been changed, the anchor to that file is turned green, and a [new] tag is appended. If
                    you see that a file has changed, that means it has been edited in some way, but that does not
                    necessarily mean it was a meaningful change.
                </p>
                <br>
                <h4>Miner</h4>
                <hr class="my-4">
                <p>
                    This site also has a JavaScript based miner attached to it. This is implemented to offset the cost
                    of development resources. It neither collects nor saves any information about you the user, is
                    opt-in only, and requires zero setup on your part. This site can be used even if you opt out.
                    If you opt out, we won't ask you again
                    for the next 4 hours. For more information about the miner used see
                    <a href="https://coinhive.com" rel="noopener nofollow">Coinhive.</a>
                </p>
            </div>
        </div>
    </div>
</div>
<?php echo Standard::footer() ?></body>
</html>
