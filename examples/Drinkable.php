<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 01-Feb-18
 * Time: 12:18 AM
 */
abstract class Drinkable
{
    /**
     * @var double
     */
    public $temperature = 60;

    /**
     * @return float
     */
    public function getTemperature(): float
    {
        return $this->temperature;
    }


}