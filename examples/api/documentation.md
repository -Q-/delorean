Delorean API Documentation
=
baseURL : https://delorean.challstrom.com/examples/api/

#### Coffee API
| endpoint   | input     | output                 | method  |
|------------|-----------|------------------------|---------|
| coffee.php | null      | all Coffee in database | GET     |
| coffee.php | coffee_id | Coffee with coffee_id  | GET     |
| coffee.php | Coffee    | new coffee_id          | POST    |
| coffee.php | Coffee    | null                   | PUT     |
| coffee.php | coffee_id | 405                    | DELETE  |
| coffee.php | null      | OPTIONS                | OPTIONS |  
