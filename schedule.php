<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 18-Jan-18
 * Time: 12:13 PM
 */

require_once __DIR__ . '/lib/Core.php';
require_once __DIR__ . '/lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

$calendarLink = 'https://calendar.google.com/calendar/embed?title=TJ%20Challstrom%20Work%20Calendar&amp;mode=WEEK&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=tchallst%40stedwards.edu&amp;color=%235229A3&amp;ctz=America%2FChicago';
?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('Schedule');
echo Standard::navbar('Schedule');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <iframe src="<?php echo $calendarLink ?>" style="border-width:0" width="100%" height="400"
                        frameborder="0" scrolling="no"></iframe>
                <small>You must be signed into Google with your St. Edward's Account to view the Technical Advisor
                    Schedule.
                </small>
            </div>
        </div>
    </div>
</div>
<script src="https://delorean.challstrom.com/scripts/hljsLoader.js"
        integrity="sha384-L02yBxfn8RHfHRN5QAniy4Rm/V0Jml0UagqMZjy00OcWwO2Yg8EhWbH+iOF+zkm0" crossorigin="anonymous"
        defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/showdown/1.8.6/showdown.min.js"
        integrity="sha384-TXBgD2Ei2XcYWHMF62BvcQr1yg9mxQXVHUDXgEzdtPH1Ez1ru8YV23tF/8mrHj5n" crossorigin="anonymous"
        defer></script>
<script src="https://delorean.challstrom.com/scripts/markdown.js"
        integrity="sha384-lp7/vWsT2CbCYIN0Bp336MLTo+l5GCb7IwdFkj/xFdQM9NMYZgYtxD9uAesbFzBp" crossorigin="anonymous"
        defer></script>
<?php echo Standard::footer() ?></body>
</html>



