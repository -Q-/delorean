<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 18-Jan-18
 * Time: 12:13 PM
 */

require_once __DIR__ . '/../lib/Core.php';
require_once __DIR__ . '/../lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('Object Oriented PHP');
echo Standard::navbar('Object Oriented PHP');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <h1 class="display-4">Object Oriented PHP</h1>
                <p class="lead">What's the Object-Oriented way to become wealthy?&nbsp
                    <small>Inheritance.</small>
                </p>
                <hr class="my-4">
                <p>Object oriented programming in PHP is easier than in other languages like Java, but
                    has some quirks of its own. Objects will be indispensable in creating your projects,
                    so get comfortable with using them!

                    Let's look at a basic object class called <code>Coffee.php</code>.
                </p>
                <pre>
                <code class="border border-white rounded">
<?php echo htmlentities(file_get_contents(__DIR__ . '/../examples/Coffee.php')); ?>
                </code>
                </pre>
                <p>
                    That doesn't look too bad does it? We have five class attributes: species, caffeine, isSpecialty,
                    and isRoasted.
                    We don't want anyone else changing our Coffee on us! So they're all private attrivutes. We've got a
                    single constructor
                    with all of the attributes as requirements. We also have getters for our five attributes.
                    Our object also extends <code>Drinkable</code>, but we'll get back to that later!
                    Now let's use our wonderful Coffee beans! First, let's invent a simple Class called <code>Roaster.php</code>.
                </p>
                <pre>
                <code class="border border-white rounded">
<?php echo htmlentities(file_get_contents(__DIR__ . '/../examples/Roaster.php')); ?>
                </code>
                </pre>
                <p>
                    This class only has one job, safely turn our raw Coffee beans into something we can use! Now we have
                    <code>Coffee</code> beans
                    and a <code>Roaster</code>. We'll also need something to drink the Coffee from! Let's invent a
                    <code>Cup</code>.
                </p>
                <pre>
                <code class="border border-white rounded">
<?php echo htmlentities(file_get_contents(__DIR__ . '/../examples/Cup.php')); ?>
                </code>
                </pre>
                <p>
                    Wait a minute, what's this <code>Drinkable</code> nonsense? Well, it would be rather silly if our
                    Cup could
                    only hold Coffee wouldn't it? Drinkable isn't all that complicated.
                </p>
                <pre>
                <code class="border border-white rounded">
                    <?php echo htmlentities(file_get_contents(__DIR__ . '/../examples/Drinkable.php')); ?>
                </code>
                </pre>
                <p>
                    Phew! I think we're <em>finally</em> ready to brew some delicious <strong><a
                                href="https://en.wikipedia.org/wiki/Coffea_arabica" target="_blank"
                                rel="nofollow noopener">Arabica</a></strong>.
                    Our <code>brewer.php</code> is what will take in Coffee, roast it, and serve up some piping hot
                    Coffee!
                </p>
                <pre>
                <code class="border border-white rounded">
<?php echo htmlentities(file_get_contents(__DIR__ . '/../examples/brewer.php')); ?>
                </code>
                </pre>
                <p>
                    We'll get to that magical <code>json_encode()</code> function in a later lesson. But let's go ahead
                    and <a target="_blank" href="https://delorean.challstrom.com/examples/brewer.php"
                           rel="help">take
                        a look at our brewer in action.</a>
                </p>
            </div>
        </div>
    </div>
</div>
<?php echo Standard::footer() ?></body>
<script src="https://delorean.challstrom.com/scripts/hljsLoader.js"
        integrity="sha384-L02yBxfn8RHfHRN5QAniy4Rm/V0Jml0UagqMZjy00OcWwO2Yg8EhWbH+iOF+zkm0" crossorigin="anonymous"
        defer></script>
</html>
