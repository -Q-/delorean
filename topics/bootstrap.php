<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 18-Jan-18
 * Time: 12:13 PM
 */

require_once __DIR__ . '/../lib/Core.php';
require_once __DIR__ . '/../lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('Bootstrap');
echo Standard::navbar('Bootstrap');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <h1 class="display-4"><img alt="Bootstrap Logo" class="img-fluid" width="7%"
                                           src="../images/Boostrap_logo.svg">&nbsp;Bootstrap</h1>
                <p class="lead">Every website ever.</p>
                <hr class="my-4">
                <p>
                    Try to <code>container</code> your enthusiasm.
                </p>
            </div>
        </div>
    </div>
</div>
<?php echo Standard::footer() ?></body>
</html>
