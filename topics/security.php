<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 18-Jan-18
 * Time: 12:13 PM
 */
require_once __DIR__ . '/../lib/Core.php';
require_once __DIR__ . '/../lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('Security');
echo Standard::navbar('Security');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <h1 class="display-4">Security</h1>
                <p class="lead"><code>403 Forbidden</code></p>
                <hr class="my-4">
                <p>
                    In today's digital world, data security is paramount. While we won't be going into
                    depth about all the details of how attacks proceed.
                    <small style="font-size: 30%;">(Too much paperwork.)</small>
                    We will be talking about how to mitigate the most common forms of attacks and vulnerabilities.
                </p>
                <ul>
                    <li><a href="#https" rel="tag">HTTPS</a></li>
                    <li><a href="#sql" rel="tag">SQL Injection</a></li>
                    <li><a href="#xss" rel="tag">Cross Site Scripting (XSS)</a></li>
                    <li><a href="#passwords" rel="tag">Passwords</a></li>
                </ul>
                <h4 class="">Secure HTTP</h4>
                <a name="https"></a>
                <p class="lead"></p>
                <hr class="my-4">
                <p>
                    HTTP or Hyper Text Transfer Protocol is a protocol which transfers strings in clear text. HTTP is
                    completely unsecured; any information passed over HTTP can be read by anyone at at time. This is
                    particularly problematic with form data, which may consist of a username and a cleartext password.
                    Enter HTTPS, a secure form of HTTP. HTTPS uses various encryption methods to hide data being
                    transmitted between a client and a web server.
                    For our solutions, HTTPS is <em>available</em> by default, meaning a client can request it. But
                    HTTP is available as well. For our websites, we'll want to tell the client that we'll only do
                    business with them if they use our HTTPS. Because our solution doesn't do this by default, we'll
                    have to do it with PHP. It's a straightforward process that has only two steps, notify the client
                    that it can't access the resource it's requesting (over HTTP), and tell the client where it can go
                    to get the resource it requires.
                </p>
                <pre>
                <code class="border border-white rounded">
public static function forceHTTPS()
{
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']) {
        return;
    } else {
        $requestURL = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('HTTP/1.1 301 Moved Permanently');
        header("Location: https://$requestURL");
        exit();
    }
}
                </code>
                </pre>
                <h4 class="">SQL Injection</h4>
                <a name="sql"></a>
                <p class="lead"></p>
                <hr class="my-4">
                <p>
                    SQL Injection attacks are attacks that use SQL syntax in input data to execute
                    malicious SQL queries on database server. SQL Injection attacks are easily mitigated by
                    scrubbing any input that is going to be used to form a query. In PHP, every library for any given
                    database driver has some form of SQL Injection mitigation function.
                </p>
                <pre>
                <code class="border border-white rounded">
public static function scrubQuery($query): string
{
    return self::getDB()->real_escape_string($query);
}
                </code>
                </pre>
                <h4 class="">Cross Site Scripting (XSS)</h4>
                <a name="xss"></a>
                <p class="lead"></p>
                <hr class="my-4">
                <p>
                    Cross site scripting attacks are attacks that inject malicious code into an otherwise benign
                    website. This often takes the form of malicious javascript being injected into un-sanitized input
                    data.
                    The easiest way to mitigate XSS attacks in PHP is to use a PHP_SANITIZE filter with a filter
                    function.
                </p>
                <pre><code class="border border-white rounded">$data = filter_var($_GET['data'], FILTER_SANITIZE_STRING);</code></pre>
                <h4 class="">Passwords</h4>
                <a name="passwords"></a>
                <p class="lead"></p>
                <hr class="my-4">
                <p>
                    Passwords are a necessary evil in today's web services landscape. Transmitting them, storing them,
                    and
                    using them for authentication, is a serious modern day challenge. There are a few rules we should
                    follow
                    to ensure our users' passwords are as safe as can be.
                </p>
                <ol>
                    <li>Never transmit passwords over a clear text channel.</li>
                    <li>Never store the actual password in any form.</li>
                    <li>Enforce strict minimum password standards.</li>
                    <li>Never transmit passwords before they are salted and hashed</li>
                </ol>
                <p>
                    With these guidelines in mind, we can begin to change our architecture accordingly.

                    This first guideline would mean that all input data needs to be submitted over HTTPS, and should
                    be transmitted using the POST or PUT methods.

                    The second guideline can be satisfied if we <em>salt</em> and <em>hash</em> our password as soon as
                    we receive it from a client. Luckily, PHP makes password salting, hashing, and verifying a breeze.
                </p>
                <pre><code class="border border-white rounded">
$saltedAndHashedPassword = password_hash($data['password'], PASSWORD_DEFAULT);
$passwordIsValid = password_verify($plainPassword, $saltedAndHashedPassword);
                    </code></pre>
                <p>
                    Passwords generated using this function will usually be exactly 60 characters long.
                    Passwords that have been salted and hashed in this way are safe to be stored or transmitted.

                    The third guideline is up to each programmer's discretion, but I would suggest enforcing the
                    following
                    password minimums.
                </p>
                <ul>
                    <li>At least 12 characters</li>
                    <li>At least 1 number</li>
                    <li>At least 1 symbol</li>
                    <li>At least 1 uppercase letter</li>
                    <li>At least 1 lowercase letter</li>
                    <li>Do not repeat the same character or character sequence more than three times in a row</li>
                </ul>
                <p>
                    The fourth guideline can be achieved by salting and hashing as described above. Proper encryption
                    should
                    still be used when transmitting salted and hashed passwords. However, the most secure transmitted
                    password
                    is the one that is never transmitted.
                </p>
            </div>
        </div>
    </div>
</div>
<script src="https://delorean.challstrom.com/scripts/hljsLoader.js"
        integrity="sha384-L02yBxfn8RHfHRN5QAniy4Rm/V0Jml0UagqMZjy00OcWwO2Yg8EhWbH+iOF+zkm0" crossorigin="anonymous"
        defer></script>
<?php echo Standard::footer() ?></body>
</html>
