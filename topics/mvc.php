<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 18-Jan-18
 * Time: 12:13 PM
 */
//Hello Naseem

require_once __DIR__ . '/../lib/Core.php';
require_once __DIR__ . '/../lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('Model-View-Controller in PHP');
echo Standard::navbar('Model-View-Controller in PHP');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <h1 class="display-4">Model-View-Controller</h1>
                <p class="lead">An overview at a model to better control your software.</p>
                <hr class="my-4">
                <p>The Model-View-Controller pattern is very simple and straight-forward.
                    A model is a class the represents an entity with attributes. Let's look at an example of a model.
                </p>
                <pre>
                <code class="border border-white rounded">
<?php echo htmlentities(file_get_contents(__DIR__ . '/../examples/Coffee.php')); ?>
                </code>
                </pre>
                <p>
                    Hey that looks familiar! It's our <code>Coffee</code> class from the <a
                            href="objectOrientedPHP.php" rel="prev">Object Orient PHP</a> example! Typically
                    your models will have attributes, a constructor that takes all of those attributes, and getters.
                    This
                    example has a mutator, but we should try to stay away from those in our models as often as possible.

                    For our purposes, we're going to <a href="http://www.dictionary.com/browse/smush" target="_blank"
                                                        rel="nofollow noopener">smush</a>
                    together
                    the View and Controller patterns to make our lives easier.
                    Now let's go ahead and look at a ViewController <code>barista.php</code>.
                    <br>
                    <em><strong>This only applies to those doing a website!</strong></em>
                </p>
                <pre>
                <code class="border border-white rounded">
<?php echo htmlentities(file_get_contents(__DIR__ . '/../examples/barista.php')); ?>
                </code>
                </pre>
                <p>
                    It's not as bad as it looks! The first thing we do in our ViewController is check to see if we have
                    any
                    incoming form data. If we do, we should handle it!
                    <br>
                    <em>There shouldn't be any business logic after the first closure: <code>?></code></em>
                    <br>
                    After the first closure, we want to have our view! A view is simply the part of MVC that a user
                    directly interacts with.
                    We may have PHP code inside the view so we can modify it as it's being displayed, but it's only
                    accessing already
                    processed information.
                    It should be noted that this is this is not a strict view controller, but it sacrifices
                    standardization
                    for easier programming.
                </p>
            </div>
        </div>
    </div>
</div>
<script src="https://delorean.challstrom.com/scripts/hljsLoader.js"
        integrity="sha384-L02yBxfn8RHfHRN5QAniy4Rm/V0Jml0UagqMZjy00OcWwO2Yg8EhWbH+iOF+zkm0" crossorigin="anonymous"
        defer></script>
<?php echo Standard::footer() ?></body>
</html>
