<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 02-Apr-18
 * Time: 05:20 PM
 */

http_response_code(403);
return;

use PHPUnit\Framework\TestCase;

class CoffeeTest extends TestCase
{

    protected $coffee = null;

    public function test__construct()
    {
        self::assertInstanceOf(Coffee::class, $this->coffee);
    }

    public function testGetCoffeeId()
    {
        self::assertAttributeEquals(1, 'coffee_id', $this->coffee);
    }

    public function testGetSpecies()
    {
        self::assertAttributeEquals('Robusta', 'species', $this->coffee);
    }

    public function testGetCaffeine()
    {
        self::assertAttributeEquals(0.0015, 'caffeine', $this->coffee);
    }

    public function testIsSpecialty()
    {
        self::assertAttributeEquals(true, 'isSpecialty', $this->coffee);
    }

    public function testIsRoasted()
    {
        self::assertAttributeEquals(false, 'isRoasted', $this->coffee);
    }

    protected function setUp()
    {
        require_once __DIR__ . '/../examples/Coffee.php';
        $this->coffee = new Coffee(1, 'Robusta', 0.0015, true, false);
    }
}
