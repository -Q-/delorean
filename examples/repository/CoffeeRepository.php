<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 12-Feb-18
 * Time: 02:41 PM
 */

//load our database driver and our coffee model whenever CoffeeRepository is loaded
require_once __DIR__ . '/../../protected/Database.php';
require_once __DIR__ . '/../../examples/Coffee.php';

class CoffeeRepository
{
    public static function getCoffeeById(int $coffee_id): Coffee
    {
        //We want to make sure to mitigate SQL injection attacks!
        $coffee_id = Database::scrubQuery($coffee_id);
        //get the raw data from the database
        $rawCoffeeDatum = Database::runQuerySingle("SELECT * FROM Coffee WHERE coffee_id='$coffee_id'");
        //our Database class will give us a one dimensional array with the result of our query, or an empty array
        //the keys of the array will be the selected columns, in our case all columns
        //now let's make sure we actually got something back
        if ($rawCoffeeDatum) {
            return new Coffee($rawCoffeeDatum['coffee_id'], $rawCoffeeDatum['species'], $rawCoffeeDatum['caffeine'], $rawCoffeeDatum['isRoasted'], $rawCoffeeDatum['isSpecialty']);
        }
        //if we couldn't find a Coffee with the given id, return null
        return null;
    }

    //this is the same as above, except it gets *all* the coffees associated with a species
    public static function getCoffeesBySpecies(string $species): array
    {
        $species = Database::scrubQuery($species);
        $rawCoffeeData = Database::runQueryAll("SELECT * FROM Coffee WHERE species='$species'");
        if ($rawCoffeeData) {
            $output = [];
            foreach ($rawCoffeeData as $rawCoffeeDatum) {
                $output[] = new Coffee($rawCoffeeDatum['coffee_id'], $rawCoffeeDatum['species'], $rawCoffeeDatum['caffeine'], $rawCoffeeDatum['isSpecialty'], $rawCoffeeDatum['isRoasted']);
            }
            return $output;
        }
        return [];
    }

    public static function getAllCoffees(): array
    {
        $rawCoffeeData = Database::runQueryAll("SELECT * FROM Coffee");
        if ($rawCoffeeData) {
            $output = [];
            foreach ($rawCoffeeData as $rawCoffeeDatum) {
                $output[] = new Coffee($rawCoffeeDatum['coffee_id'], $rawCoffeeDatum['species'], $rawCoffeeDatum['caffeine'], $rawCoffeeDatum['isSpecialty'], $rawCoffeeDatum['isRoasted']);
            }
            return $output;
        }
        return [];
    }

    public static function insertCoffee(Coffee $coffee): int
    {
        $species = Database::scrubQuery($coffee->getSpecies());
        $caffeine = Database::scrubQuery($coffee->getCaffeine());
        $isRoasted = Database::scrubQuery($coffee->isRoasted());
        $isSpecialty = Database::scrubQuery($coffee->isSpecialty());

        $isRoasted = $isRoasted ? 1 : 0;
        $isSpecialty = $isSpecialty ? 1 : 0;

        $result = Database::runQuerySingle("INSERT INTO Coffee(species, caffeine, isRoasted, isSpecialty) VALUES ('$species', '$caffeine', '$isRoasted', '$isSpecialty')");
        if ($result) {
            return Database::getLastKey();
        } else {
            return -1;
        }

    }

    public static function updateCoffee(Coffee $coffee): bool
    {
        $coffee_id = Database::scrubQuery($coffee->getCoffeeId());
        $species = Database::scrubQuery($coffee->getSpecies());
        $caffeine = Database::scrubQuery($coffee->getCaffeine());
        $isRoasted = Database::scrubQuery($coffee->isRoasted());
        $isSpecialty = Database::scrubQuery($coffee->isSpecialty());

        return Database::runQuerySingle("UPDATE Coffee SET species = '$species', caffeine = '$caffeine', isRoasted = '$isRoasted', isSpecialty = '$isSpecialty' WHERE coffee_id = '$coffee_id'");

    }

    public static function coffeeExistsById($coffee_id): bool
    {
        $coffee_id = Database::scrubQuery($coffee_id);
        return isset(Database::runQuerySingle("SELECT coffee_id FROM Coffee WHERE coffee_id='$coffee_id'")['coffee_id']);
    }
}