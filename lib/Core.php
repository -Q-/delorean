<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 15-Jan-18
 * Time: 02:33 PM
 */
class Core
{
    public static function setCache(bool $shouldCache)
    {
        if ($shouldCache) {
            header('Cache-Control: public, max-age=86400');
        } else {
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
        }
    }

    public static function forceHTTPS()
    {
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']) {
            return;
        } else {
            $requestURL = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            header('HTTP/1.1 301 Moved Permanently');
            header("Location: https://$requestURL");
            exit();
        }

    }
}