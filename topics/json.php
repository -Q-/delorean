<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 18-Jan-18
 * Time: 12:13 PM
 */

require_once __DIR__ . '/../lib/Core.php';
require_once __DIR__ . '/../lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('JSON');
echo Standard::navbar('JSON');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <h1 class="display-4">JSON</h1>
                <p class="lead"><a href="https://www.json.org/" target="_blank" rel="nofollow">JSON</a> not <a
                            href="https://www.linkedin.com/in/jmorris7/" target="_blank"
                            rel="nofollow noopener">JSON</a></p>
                <hr class="my-4">
                Now what is JSON?<br>
                <del>&quot;JSON (JavaScript Object Notation) is a lightweight data-interchange format.&quot;</del>
                <br>
                Basically, JSON is a specially formatted human-readable string that makes transferring information
                between hosts straightforward.
                So what does this special string look like?
                <pre>
                <code class="border border-white rounded">
<?php
require_once __DIR__ . '/../examples/Coffee.php';
$coffee = new Coffee(0, 'Arabica', 0.15, true, false);
echo json_encode($coffee, JSON_PRETTY_PRINT);
?>
                </code>
                </pre>
                <p>
                    You can think about JSON strings like associative arrays in PHP. In fact, when you <em>decode</em>
                    a JSON string in PHP, it gets turned into an associative array! Let's look at an example of PHP
                    decoding a JSON string, scrubbing it, creating a model, and encoding a model to a JSON string.
                </p>
                <pre>
                <code class="border border-white rounded">
<?php echo htmlentities(file_get_contents(__DIR__ . '/../examples/remoteCoffeeRoaster.php')); ?>
                </code>
                </pre>
                <p>
                    You can test out this script <a
                            href="https://delorean.challstrom.com/examples/remoteCoffeeRoaster.php"
                            target="_blank" rel="help">here.</a>
                    Almost all Classes and Objects in PHP are serializable into JSON by default.
                    <code>json_encode()</code>
                    will take all of the <strong>public</strong> attributes of a class or object and turn them into
                    key => value pairs. It will also serialize any superclasses or any attribute classes. <em>If you
                        want to add custom functionality to <code>json_encode()</code> you can implement the
                        <code>JsonSerializable</code> interface <a
                                href="https://secure.php.net/manual/en/class.jsonserializable.php" target="_blank"
                                rel="nofollow noopener">described
                            here.</a></em>
                </p>
            </div>
        </div>
    </div>
</div>
<script src="https://delorean.challstrom.com/scripts/hljsLoader.js"
        integrity="sha384-L02yBxfn8RHfHRN5QAniy4Rm/V0Jml0UagqMZjy00OcWwO2Yg8EhWbH+iOF+zkm0" crossorigin="anonymous"
        defer></script>
<?php echo Standard::footer() ?></body>
</html>
