<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 01-Feb-18
 * Time: 12:18 AM
 */
require_once __DIR__ . '/Drinkable.php';

class Cup
{
    /**
     * @var array
     */
    public $contents;
    /**
     * @var double
     */
    public $temperature;

    /**
     * Adds Drinkables to contents and recalculates temperature
     * @param Drinkable $drinkable
     */
    public function addContents(Drinkable $drinkable)
    {
        $this->contents[] = $drinkable;
        //let's calculate the new temperature
        $sum = 0;
        foreach ($this->contents as $content) {
            $sum += $content->getTemperature();
        }
        $this->temperature = $sum / count($this->contents);
    }


}