<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 19-Feb-18
 * Time: 03:48 PM
 */
require_once __DIR__ . '/../examples/Coffee.php';
//We have to get JSON data from php://input because it's not your usual content-type
//Then we have to decode it to turn the string into an array
$jsonData = json_decode(file_get_contents('php://input'), true);
//
//
//this line is for *testing purposes ONLY,* it's here so we can see this script run properly
$jsonData = json_decode(json_encode(new Coffee(0, 'Arabica', 0.15, true, false)), true);
//
//
//
//let's make sure we actually got *something*
if (!$jsonData) {
    echo 'No data received!';
    http_response_code(400);
    exit(-1);
}
//then we filter the values of our new array to make sure malicious code hasn't been entered
$jsonData = filter_var_array($jsonData, FILTER_SANITIZE_STRING);
//now we should go through our json array to make sure all of the keys for our models are set
$errorCount = 0;
if (!isset($jsonData['species'])) {
    echo 'ERROR! (species) was not set!';
    $errorCount++;
}
if (!isset($jsonData['caffeine'])) {
    echo 'ERROR! (caffeine) was not set!';
    $errorCount++;
}
if (!isset($jsonData['isSpecialty'])) {
    echo 'ERROR! (isSpecialty) was not set!';
    $errorCount++;
}
if (!isset($jsonData['isRoasted'])) {
    echo 'ERROR! (isRoasted) was not set!';
    $errorCount++;
}
if ($errorCount) {
    echo "Total Errors: $errorCount";
    http_response_code(400);
    exit(-1);
}
//now let's try to construct a Coffee model with our JSON data
$coffee = new Coffee($jsonData['coffee_id'], $jsonData['species'], $jsonData['caffeine'], $jsonData['isSpecialty'], $jsonData['isRoasted']);
//now let's go ahead and roast our delicious coffee
require_once __DIR__ . '/../examples/Roaster.php';
Roaster::roastCoffee($coffee);
//now we have roasted the coffee we were sent, so let's return it to whomever asked us to roast it
//first we have to make sure we're returning *JSON*
header('Content-Type: application/json');
//then all we have to do is echo out a json string! Almost all PHP classes and objects are serializable into JSON by default
echo json_encode($coffee);
