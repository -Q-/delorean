<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 20-Mar-18
 * Time: 10:17 PM
 */

header('Content-Type: application/php');
header("Content-disposition: attachment;filename=EasyDatabase.php");
echo file_get_contents(__DIR__ . '/examples/EasyDatabase.php');