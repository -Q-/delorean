<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 18-Jan-18
 * Time: 12:13 PM
 */

require_once __DIR__ . '/../lib/Core.php';
require_once __DIR__ . '/../lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('Name Generation');
echo Standard::navbar('Name Generation');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <h1 class="display-4">Name Generation</h1>
                <p class="lead">Inspiration as a Service.</p>
                <hr class="my-4">
                <script src="https://delorean.challstrom.com/scripts/nameGenerator.js"
                        integrity="sha384-N8JADdPwee3HaoN4JaUOnQ29DQY86dVGZLav7u9Ke2I+W6/Im/tsYy1yU6BCbiPR"
                        crossorigin="anonymous" defer></script>
                <form>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary" onclick="return populateSingleName();">Generate
                            Name
                        </button>
                        <button type="button" class="btn btn-danger" onclick="return populateManyNames();"> << Mash the
                            button
                        </button>
                    </div>
                    <div class="form-group">
                        <textarea id="generatedName" class="form-control" title="Generated Name" readonly></textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo Standard::footer() ?></body>
</html>
