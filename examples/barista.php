<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 05-Feb-18
 * Time: 10:48 AM
 */

$errors = [];
$cup = null;
$speciesAvailable = ['Arabica', 'Liberica', 'Robusta'];
if (isset($_POST['makeCoffee'])) {
    //make sure our form was actually filled out
    if (!isset($_POST['species'])) $errors[] = 'Species not set!';
    if (!isset($_POST['shots'])) $errors[] = 'Shots not set!';

    //remove any bad characters
    $species = filter_var($_POST['species'], FILTER_SANITIZE_STRING);
    $shots = filter_var($_POST['shots'], FILTER_SANITIZE_NUMBER_INT);

    //a little bit of server side data validation
    if (!is_numeric($shots)) $errors[] = 'Shots was not set to a number';
    if (!in_array($species, $speciesAvailable)) $errors[] = 'Species is not available!';

    //if we have errors, don't brew the coffee!
    if (!count($errors)) {
        //only load the libraries if we're going to use them!

        require_once __DIR__ . '/Coffee.php';
        require_once __DIR__ . '/Cup.php';
        $cup = new Cup();
        $coffee = new Coffee(0, $species, 0.12, true, true);
        for ($i = 0; $i < $shots; $i++) {
            $cup->addContents($coffee);
        }
    }
}

//No logic after the first closure!
?>

<!DOCTYPE html>

<html lang="en">
<head>
    <title>DeLorean | Barista Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
            integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
          integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="https://delorean.challstrom.com/styles/delorean.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <form action="barista.php" method="post">
                    <div class="form-group">
                        <label for="species"></label>
                        <select class="form-control" id="species" name="species">
                            <?php
                            foreach ($speciesAvailable as $s) {
                                echo "<option value='$s'>$s</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="shots"></label>
                        <input type="number" min="1" class="form-control" id="shots" name="shots" value="1" required>
                    </div>
                    <button type="submit" class="btn btn-primary" id="makeCoffee" name="makeCoffee">Order Cup of
                        Coffee
                    </button>
                </form>
            </div>
        </div>
    </div>
    <?php
    if ($errors) {
        echo '    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                ';
        foreach ($errors as $error) {
            echo $error . '<br>';
        }
        echo '            </div>
        </div>
    </div>';
    }
    if ($cup) {
        echo '    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                ';
        echo 'Your Cup of coffee is ready! It has ' . count($cup->contents) . ' shots of ' . $cup->contents[0]->getSpecies() . ' in it!';
        echo '            </div>
        </div>
    </div>';
    }
    ?>
</div>
<?php echo Standard::footer() ?></body>
</html>


