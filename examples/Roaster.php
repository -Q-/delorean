<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 01-Feb-18
 * Time: 12:02 AM
 */
//What did those poor beans ever do to you?
class Roaster
{
    public static function roastCoffee(Coffee $coffee)
    {
        //We don't want to roast our coffee twice!
        if (!$coffee->isRoasted()) {
            $coffee->roast();
        }
    }
}