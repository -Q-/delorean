<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 18-Jan-18
 * Time: 12:13 PM
 */

require_once __DIR__ . '/../lib/Core.php';
require_once __DIR__ . '/../lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('Swift');
echo Standard::navbar('Swift');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <h1 class="display-4"><img alt="Bootstrap Logo" class="img-fluid" width="7%"
                                           src="../images/Swift_logo.svg">&nbsp;Swift</h1>
                <p class="lead"><a href="https://www.merriam-webster.com/dictionary/oxymoron" target="_blank"
                                   rel="nofollow noopener">See
                        Oxymoron &rarr;</a></p>
                <hr class="my-4">
                <p>
                    <a href="../examples/CoffeeSwift.7z" type="application/x-7z-compressed.">Download the XCode project
                        here.</a>
                </p>
            </div>
        </div>
    </div>
</div>
<?php echo Standard::footer() ?>
</body>
</html>
