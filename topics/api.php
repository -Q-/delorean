<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 18-Jan-18
 * Time: 12:13 PM
 */

require_once __DIR__ . '/../lib/Core.php';
require_once __DIR__ . '/../lib/Standard.php';

Core::forceHTTPS();
Core::setCache(true);

?>

<!DOCTYPE html>

<html lang="en">
<?php echo Standard::head('API');
echo Standard::navbar('API');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="jumbotron">
                <h1 class="display-4"><strong>A</strong>pplication <strong>P</strong>rogramming <strong>I</strong>nterface
                </h1>
                <p class="lead">So RESTful.</p>
                <hr class="my-4">
                <p>
                    In the world of web services, the API is one of the most important high level concepts to
                    understand. Luckily,
                    it's also straightforward.<br>
                    APIs have a few key ideas:
                </p>
                <ul>
                    <li><a href="#apiEndpointScripts" rel="tag">API Endpoint Scripts</a></li>
                    <li><a href="#inputs" rel="tag">Inputs</a></li>
                    <li><a href="#outputs" rel="tag">Outputs</a></li>
                    <li><a href="#httpsMethods" rel="tag">HTTPS Methods</a></li>
                    <li><a href="#documentation" rel="tag">Documentation</a></li>
                </ul>
                <h4 class="">API Endpoint Scripts</h4>
                <a name="apiEndpointScripts"></a>
                <p class="lead"></p>
                <hr class="my-4">
                <p>The <em>endpoint</em> of an API is a script that handles your API requests.
                    For many web services, this is located at the <code>/api</code> location; this is where you ought to
                    have all of your APIs as well. The endpoints that you have will tend to follow the models you have,
                    so
                    if you have taxi model you will probably have a <code>/api/taxi.php</code> endpoint. In our system,
                    we may have API endpoints that correspond to different remote procedure calls. Remote procedure
                    calls,
                    or RPC, is like an API - but it causes different functionality to occur.
                    Each endpoint will have its own set of inputs, outputs, and supported methods.
                    Our example endpoint is <a
                            href="https://delorean.challstrom.com/examples/api/coffee.php"
                            target="_blank" rel="help">here.</a>
                </p>
                <h4 class="">Inputs</h4>
                <a name="inputs"></a>
                <p class="lead"></p>
                <hr class="my-4">
                <p>
                    The <em>inputs</em> of an API endpoint are the expected and valid data that can be sent with a
                    request. The inputs can be both the GET or POSt keys that are sent, along with any payload
                    in the HTTP body. A payload is a section of data in a request. In our system, the payload will be
                    JSON strings.

                </p>
                <h4 class="">Outputs</h4>
                <a name="outputs"></a>
                <p class="lead"></p>
                <hr class="my-4">
                <p>
                    The <em>outputs</em> on an API refer to the data that an API returns given an input. For example,
                    in our <a
                            href="https://delorean.challstrom.com/examples/api/coffee.php"
                            target="_blank" rel="help">example API endpoint</a>, if a GET request is sent (the default
                    request your
                    web browser sends
                    to get a web page), our API will return every <code>Coffee</code> model stored in our database.
                </p>
                <h4 class="">HTTPS Methods</h4>
                <a name="httpsMethods"></a>
                <p class="lead"></p>
                <hr class="my-4">
                <p>
                    The HTTPS methods for an API are well defined methods for interacting with an API. In our system,
                    we'll use GET, POST, PUT, and DELETE. (You can invent your own methods - but it's not recommended).
                </p>
                <ul>
                    <li>
                        GET is used when we are attempting to retrieve a resource from a server.
                    </li>
                    <li>
                        POST is used when we are trying to create a <strong>new</strong> resource on a server.
                    </li>
                    <li>
                        PUT is used when we are trying to update or overwrite a resource on a server.
                    </li>
                    <li>
                        DELETE is used to remove a resource from a server.
                    </li>
                </ul>
                <p>
                    Adjacent to HTTPS methods are HTTP response codes. The most common codes we'll use are 200, 204, and
                    405.
                </p>
                <ul>
                    <li>
                        200 means that an API request completed successfully
                    </li>
                    <li>
                        204 means that the HTTPS method cannot be used for that API
                    </li>
                    <li>
                        405 means that a particular HTTPS method can be used, but has not yet been implemented
                    </li>
                </ul>
                <p>
                    In <a
                            href="https://delorean.challstrom.com/examples/api/coffee.php"
                            target="_blank" rel="help">our example API</a>, we use all of the methods described as well
                    as the
                    response codes described.
                    Here is the PHP for our example API:
                </p>
                <pre>
                <code class="border border-white rounded">
<?php echo htmlentities(file_get_contents(__DIR__ . '/../examples/api/coffee.php')); ?>
                </code>
                </pre>
                <p>
                    <a href="https://insomnia.rest" target="_blank" rel="nofollow noopener">Insomnia</a> is a very
                    useful tool to help test your
                    APIs. It can run various HTTPS requests
                    to your APIs. Once you have Insomnia, you can use <a
                            href="https://delorean.challstrom.com/examples/deloreanInsomniaCoffee.json"
                            target="_blank" rel="help">this insomnia export</a> to make various requests
                    to the <code>Coffee</code> API described above.
                </p>
                <h4 class="">Documentation</h4>
                <a name="documentation"></a>
                <p class="lead"></p>
                <hr class="my-4">
                <p>
                    You should document your APIs as you implement them. Every method, input, and output
                    for every endpoint should be documented. Also <strong>please include the baseURL to your
                        APIs.</strong>
                    The baseURL is the url that comes before the endpoint. See below.
                    For your documentation, I highly suggest you create a text based file that organizes your
                    information
                    in a structured manner. <a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet"
                                               target="_blank" rel="nofollow noopener">Markdown</a>
                    or HTML are excellent solutions for this documentation. Markdown is automatically rendered by
                    BitBucket into a visually
                    appealing format. The API documentation for our Coffee Example is <a
                            href="https://bitbucket.org/-Q-/delorean/src/master/examples/api/documentation.md"
                            target="_blank" rel="help nofollow noopener">here.</a>
                    A helpful tool for creating Markdown tables like you see in that example is available <a
                            href="https://www.tablesgenerator.com/markdown_tables" target="_blank"
                            rel="nofollow noopener">here.</a>
                    This is the raw Markdown for the coffee API documentation:
                </p>
                <pre>
                <code class="border border-white rounded hljs markdown">
<?php echo htmlentities(file_get_contents(__DIR__ . '/../examples/api/documentation.md')); ?>
                </code>
                </pre>
                And the GFM rendered Markdown:

                <div id="renderedMarkdown" class="table table-striped">
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://delorean.challstrom.com/scripts/hljsLoader.js"
        integrity="sha384-L02yBxfn8RHfHRN5QAniy4Rm/V0Jml0UagqMZjy00OcWwO2Yg8EhWbH+iOF+zkm0" crossorigin="anonymous"
        defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/showdown/1.8.6/showdown.min.js"
        integrity="sha384-TXBgD2Ei2XcYWHMF62BvcQr1yg9mxQXVHUDXgEzdtPH1Ez1ru8YV23tF/8mrHj5n" crossorigin="anonymous"
        defer></script>
<script src="https://delorean.challstrom.com/scripts/markdown.js"
        integrity="sha384-lp7/vWsT2CbCYIN0Bp336MLTo+l5GCb7IwdFkj/xFdQM9NMYZgYtxD9uAesbFzBp" crossorigin="anonymous"
        defer></script>
<?php echo Standard::footer() ?></body>
</html>

