/*
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

$(window).on('scroll', function () {
    let s = $(window).scrollTop(),
        d = $(document).height(),
        c = $(window).height();

    let scrollPercent = (s / (d - c)) * 100;
    if (0 <= scrollPercent && scrollPercent <= 100) {
        const pageProgress = $("#pageProgress");
        pageProgress.css('width', scrollPercent + '%');
        if (scrollPercent > 98) {
            pageProgress.addClass('bg-success');
        }
        pageProgress.html(Math.ceil(scrollPercent) + '%');
    }
});