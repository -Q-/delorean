<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 21-Feb-18
 * Time: 01:53 PM
 */

##The Response library makes it very easy to encapsulate data in a tertiary structure to build the json string more easily
require_once __DIR__ . "/../../lib/Response.php";
require_once __DIR__ . "/../repository/CoffeeRepository.php";

$response = new Response();
$method = filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING);

switch ($method) {
    case 'GET':
        if (isset($_GET['coffee_id'])) {
            $coffee_id = filter_var($_GET['coffee_id'], FILTER_SANITIZE_STRING);
            $coffee = CoffeeRepository::getCoffeeById($coffee_id);
            $response->pushData($coffee);
            http_response_code(200);
            $response->echoJSONString();
        } else {
            foreach (CoffeeRepository::getAllCoffees() as $coffee) {
                $response->pushData($coffee);
            }
            http_response_code(200);
            $response->echoJSONString();
        }
        break;
    case 'PUT':
        $data = json_decode(file_get_contents("php://input"), true);
        $data = filter_var_array($data, FILTER_SANITIZE_STRING);
        if (!isset($data['coffee_id'])) {
            $response->pushError("(coffee_id) was not set for $method!");
        }
        if (!isset($data['species'])) {
            $response->pushError("(species) was not set for $method!");
        }
        if (!isset($data['caffeine'])) {
            $response->pushError("(caffeine) was not set for $method!");
        }
        if (!isset($data['isRoasted'])) {
            $response->pushError("(isRoasted) was not set for $method!");
        }
        if (!isset($data['isSpecialty'])) {
            $response->pushError("(isSpecialty) was not set for $method!");
        }
        if (!$response->getErrorCount()) {
            if (CoffeeRepository::coffeeExistsById($data['coffee_id'])) {
                $coffee = new Coffee($data['coffee_id'], $data['species'], $data['caffeine'], $data['isSpecialty'], $data['isRoasted']);
                $querySuccess = CoffeeRepository::updateCoffee($coffee);
                if ($querySuccess) {
                    http_response_code(200);
                } else {
                    http_response_code(400);
                }
            } else {
                $response->pushError('Coffee with id ' . $data['coffee_id'] . ' does not exist!');
                http_response_code(404);
            }
        } else {
            http_response_code(400);
        }
        $response->echoJSONString();
        break;
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        $data = filter_var_array($data, FILTER_SANITIZE_STRING);
        if (!isset($data['species'])) {
            $response->pushError("(species) was not set for $method!");
        }
        if (!isset($data['caffeine'])) {
            $response->pushError("(caffeine) was not set for $method!");
        }
        if (!isset($data['isRoasted'])) {
            $response->pushError("(isRoasted) was not set for $method!");
        }
        if (!isset($data['isSpecialty'])) {
            $response->pushError("(isSpecialty) was not set for $method!");
        }
        if (!$response->getErrorCount()) {
            $coffee = new Coffee(-1, $data['species'], $data['caffeine'], boolval($data['isRoasted']), boolval($data['isSpecialty']));
            $querySuccess = CoffeeRepository::insertCoffee($coffee);
            if ($querySuccess) {
                http_response_code(200);
                $response->pushData(['coffee_id' => Database::getLastKey()]);
            } else {
                $response->pushError("Coffee could not be inserted successfully.");
                http_response_code(400);
            }
        } else {
            http_response_code(400);
        }
        $response->echoJSONString();
        break;
    case 'DELETE':
        http_response_code(405);
        $response->echoJSONString();
        break;
    case 'OPTIONS':
        header('Allow: OPTIONS, GET, POST, PUT, DELETE');
        break;
    default:
        http_response_code(204);
        $response->echoJSONString();
}