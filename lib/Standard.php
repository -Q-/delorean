<?php
/**
 * Copyright (c) 2018. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 15-Jan-18
 * Time: 03:03 PM
 */
class Standard
{
    public static $maintenanceMode = false;

    public static function head(string $pageTitle)
    {
        if (Standard::$maintenanceMode) {
            if (!isset($_GET['override'])) {
                header('HTTP/1.1 503 Service Temporarily Unavailable', true, 503);
                header('Retry-After: 300');
//                header("Location: https://delorean.challstrom.com/static/maintenance.html");
                echo file_get_contents(__DIR__ . '/../static/maintenance.html');
                exit();
            } else {
                echo "<div style='width: 100%; background: red; text-align: center'><strong>WARNING!</strong> You are overriding the site's maintenance mode! Unexpected behavior may occur!</div>";
            }
        }
        header('Link: </styles/delorean.css>; as=style; rel=preload', false);
        header('Link: </scripts/authedmine.min.js>; as=script; rel=preload', false);
        header('Link: </scripts/qrcode.min.js>; as=script; rel=preload', false);
        header('Link: </images/deloreanLogo.svg>; as=image; rel=preload', false);
        require_once __DIR__ . '/../FileStatus.php';
        return "<head>
    <title>DeLorean | $pageTitle</title>" . "
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <meta name='theme-color' content='#C3B3FF'>
    <meta name='description' content=\"St. Edward's University Computer Science Technical Advisor Wiki\">
    <meta name='keywords' content='wiki,technical,bootstrap,st edwards, university, software, engineering, COSC, 3339, 4157'>
    <meta name='author' content='TJ Challstrom'>
    <meta name='robots' content='index/nofollow'>
    <meta name=\"google-site-verification\" content=\"v0JAmUVbwjEQFg1qS5aZfZE5RHpQbvTpiwBq8D9H4NE\" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-118584951-1\"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-118584951-1');
    </script>

    " . '
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous" defer></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
            integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
            crossorigin="anonymous" defer></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
          integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="https://delorean.challstrom.com/styles/delorean.css">
    <link rel="shortcut icon" type="image/png" href="https://delorean.challstrom.com/images/deloreanLogo.png">
    <link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js" integrity="sha384-ZeLYJ2PNSQjvogWP559CDAf02Qb8FE5OyQicqtz/+UhZutbrwyr87Be7NPH/RgyC" crossorigin="anonymous" defer></script>
    <script src="https://delorean.challstrom.com/scripts/scrollProgress.js"defer></script>
    <script src="https://delorean.challstrom.com/scripts/authedmine.min.js"></script>
    <script type="application/javascript" async>
        document.addEventListener("DOMContentLoaded", function() {
              let miner = new CoinHive.Anonymous(\'w8UiG11dncynzYruCTGZwn2PMv8KXixk\', {throttle: 0.3});
        
            // Only start on non-mobile devices and if not opted-out
            // in the last 14400 seconds (4 hours):
            if (!miner.isMobile() && !miner.didOptOut(14400)) {
                miner.start();
            }
        })
    </script>
</head>';
    }

    public static function navbar(string $currentPage)
    {
        $pages = ['Home' => 'https://delorean.challstrom.com/index.php', 'Bitbucket' => 'https://bitbucket.org/-Q-/delorean', 'Schedule' => 'https://delorean.challstrom.com/schedule.php'];
        $pages[$currentPage] = 'active';
        $out = '<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">
    <img src="https://delorean.challstrom.com/images/deloreanLogo.svg" width="30" height="30" class="d-inline-block align-top" alt="">
    DeLorean
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      ';
        foreach ($pages as $page => $href) {
            if ($href == 'active') {
                $out .= "<li class=\"nav-item active\">
        <a class=\"nav-link\" href=\"#\" onclick='window.location.reload()'>$page <span class=\"sr-only\">(current)</span></a>
      </li>";
            } elseif ($href == 'disabled') {
                $out .= "      <li class=\"nav-item\">
        <a class=\"nav-link disabled\" href=\"#\">$page</a>
      </li>";
            } else {
                $out .= "<li class=\"nav-item\">
        <a class=\"nav-link\" href=\"$href\">$page</a>
      </li>";
            }
        }
        $out .= '<li class="nav-item">' . FileStatus::getAnchor('https://delorean.challstrom.com/getEasyDatabase.php', 'EasyDatabase.php', 'nav-link', '_blank') . '</li>';
//        $nameGenerationClass = FileStatus::getClass(__DIR__ . '/../topics/nameGeneration.php');
//        $objectOrientPHPClass = FileStatus::getClass(__DIR__ . '/../topics/objectOrientedPHP.php');
//        $mvcClass = FileStatus::getClass(__DIR__ . '/../topics/mvc.php');
//        $bootstrapClass = FileStatus::getClass(__DIR__ . '/../topics/bootstrap.php');
//        $dataLayerClass = FileStatus::getClass(__DIR__ . '/../topics/dataLayer.php');
//        $jsonClass = FileStatus::getClass(__DIR__ . '/../topics/json.php');
//        $apiClass = FileStatus::getClass(__DIR__ . '/../topics/api.php');
//        $securityClass = FileStatus::getClass(__DIR__ . '/../topics/security.php');
//        $topicsChanged = $nameGenerationClass | $objectOrientPHPClass | $mvcClass | $bootstrapClass | $dataLayerClass | $jsonClass | $apiClass | $securityClass;
//        $dropdownClass = $topicsChanged ? 'changed' : '';
        $out .= "
<li class='nav-item dropdown'>
        <a class='nav-link dropdown-toggle' href='#' id='navbarDropdownTopic' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          Topics<span class='badge' style='display: none;'>New</span>
        </a>
        <div class='dropdown-menu' aria-labelledby='navbarDropdownTopic'> ";
        $out .= FileStatus::getAnchor('https://delorean.challstrom.com/topics/nameGeneration.php', 'Name Generation', 'dropdown-item', '_self');
        $out .= FileStatus::getAnchor('https://delorean.challstrom.com/topics/objectOrientedPHP.php', 'Object Oriented PHP', 'dropdown-item', '_self');
        $out .= FileStatus::getAnchor('https://delorean.challstrom.com/topics/mvc.php', 'Model-View-Controller', 'dropdown-item', '_self');
        $out .= FileStatus::getAnchor('https://delorean.challstrom.com/topics/bootstrap.php', 'Bootstrap', 'dropdown-item', '_self');
        $out .= FileStatus::getAnchor('https://delorean.challstrom.com/topics/dataLayer.php', 'The Data Layer', 'dropdown-item', '_self');
        $out .= FileStatus::getAnchor('https://delorean.challstrom.com/topics/json.php', 'JSON', 'dropdown-item', '_self');
        $out .= FileStatus::getAnchor('https://delorean.challstrom.com/topics/api.php', 'APIs', 'dropdown-item', '_self');
        $out .= FileStatus::getAnchor('https://delorean.challstrom.com/topics/security.php', 'Security', 'dropdown-item', '_self');
        $out .= FileStatus::getAnchor('https://delorean.challstrom.com/topics/javascript.php', 'Javascript', 'dropdown-item', '_self');
        $out .= FileStatus::getAnchor('https://delorean.challstrom.com/topics/swift.php', 'Swift', 'dropdown-item', '_self');
        $out .= FileStatus::getAnchor('https://delorean.challstrom.com/topics/phpunit.php', 'PHPUnit', 'dropdown-item', '_self');
        $out .= '
          <div class=\'dropdown-divider\'></div>
          <a class=\'dropdown-item\' href=\'https://youtu.be/dQw4w9WgXcQ\' target=\'_blank\'>Final Exam Answers</a>
          <a class=\'dropdown-item\' href=\'mailto:tchallst@stedwards.edu?subject=Software Engineering I\' target=\'_blank\'>Email your Technical Advisor</a>

      </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownWebsites" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Websites
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdownWebsites">
      <a class="dropdown-item" href="https://jsherma1.create.stedwards.edu/WeGo/#" target="_blank" rel="noopener">21 Savage</a>
      <a class="dropdown-item" href="https://rgarci13.create.stedwards.edu/cosc2328/index.php" target="_blank" rel="noopener">Sleepy Whiskers</a>
      <a class="dropdown-item" href="https://athomas9.create.stedwards.edu/roboTaxi/" target="_blank" rel="noopener">Starkiller Base</a>
      <a class="dropdown-item" href="https://aalfarha.create.stedwards.edu/software/demand/index.php" target="_blank" rel="noopener">T.A.X.I.</a>
      <a class="dropdown-item" href="https://jzamora4.create.stedwards.edu/roboTaxi/homepage/" target="_blank" rel="noopener">Z-Go</a>
      <a class="dropdown-item" href="https://ovahsen.create.stedwards.edu/software1/" target="_blank" rel="noopener">Turtles</a>
      <a class="dropdown-item" href="https://nraad1.create.stedwards.edu/" target="_blank" rel="noopener">WeGoDown</a>
      <a class="dropdown-item" href="https://aalcanta.create.stedwards.edu/zum/MVC/" target="_blank" rel="noopener">Zume</a>
      <a class="dropdown-item" href="https://nluna.create.stedwards.edu/wegoats/index.php" target="_blank" rel="noopener">We Goats</a>
      </div>
</li>
    </ul>
        <div class="progress" style="width: 20%; margin-right: 5rem;">
        <div id="pageProgress" class="progress-bar w-0" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
    <div id="qrcode" class="qrcode"></div>
  </div>
</nav>';
        $out .= '<div id="qrcode"></div>

<script src="https://delorean.challstrom.com/scripts/qrcode.min.js"defer></script>
';
        return $out;
    }

    public static function footer(): string
    {
        $currentYear = date("Y");
        $out = '
        <footer class="footer">
    <div class="container">
        ' . "<p class='text-muted small'>Challstrom © 2017-$currentYear</p>" . '
    </div>
</footer>';
        return $out;
    }
}